---
title: DebConf20 schedule is up
---

The DebConf team is proud to announce that the [DebConf20 Online
schedule](/schedule/) is now published. There are 7 days of activities,
running from 10:00 to 01:00 UTC.

There may still be minor changes to the schedule as we hear back from speakers,
but most of the program should be final at this point.

See you online for DebConf20!
