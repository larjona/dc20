---
title: DebConf20 Moves Online
---

The DebConf team has had to take the hard decision that DebConf 20
cannot happen in-person, in Haifa, in August, as originally planned.
This decision is based on the status of the venue in Haifa, the local
team's view of the local health situation, the existing travel
restrictions and the [results of the survey we ran][0].

[0]: https://salsa.debian.org/debconf-team/public/data/dc20/-/tree/master/survey

What are we doing instead?

We're holding DebConf 20 online!

We can still get together to share our ideas, discuss plans in Birds of
a Feather sessions, and eat cheese, from the safety of our desks at
home.

So, please [submit your talk, sprint, and BoF proposals][1] for DebConf
20 Online.

[1]: https://debconf20.debconf.org/cfp/

It will be held within the same dates, as before, 23-29 August.
We expect the event to be significantly shorter than a usual DebCamp +
DebConf, but that will depend on the volume of proposals we receive.

Hopefully in 2021 we can once again hold conferences in person, with
DebConf 21 taking place in Haifa.
In that case, all of the planned DebConfs would be held a year later
than originally scheduled: 2022 in Kosovo and 2023 in Kochi, India.

See you online in August!
