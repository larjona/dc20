---
title: Coronavirus update
---

As of March 2020, Israel has forbidden all non-nationals from entering
Israel, and all nationals returning must enter 14 days of
self-quarantine.
The entire country is, at the time of writing, more or less under
curfew.
As it stands, it is not certain whether the conference can take place.
Bear in mind, however, that the conference is still five months away,
and the situation is rapidly evolving.

The planning committee has not yet made any decision, and is working,
for the time being, under the assumption that the conference will take
place as originally planned.

The deadline for deciding whether to postpone, cancel, or change the
format of the conference is at the beginning of June.

Please continue to submit talks, and watch the [debconf-announce
mailing list](https://lists.debian.org/debconf-announce/) for updates.

See [the DebConf 20 FAQ](https://wiki.debian.org/DebConf/20/Faq) for any
related questions you have.
