from wafer.schedule.admin import SCHEDULE_ITEM_VALIDATORS

# Workaround strictness of wafer schedule validation
non_contiguous = None
i = 0
for _, err_type, _ in SCHEDULE_ITEM_VALIDATORS:
    if err_type == "non_contiguous":
        non_contiguous = i
        break
    i += 1
if non_contiguous is not None:
    SCHEDULE_ITEM_VALIDATORS.pop(non_contiguous)

