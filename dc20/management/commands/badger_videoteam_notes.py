# -*- coding: utf-8 -*-
from django.contrib.auth.models import Group
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.template import engines


SUBJECT = "DebConf Video Team Director and Talkmeister notes"

TEMPLATE = """\
Dear {{ name }},

Thanks for helping out the DebConf video Team.

The video team IRC channel is a very noisy place, so you probably aren't seeing
all the feedback.

We noticed some common issues during talks, and we have documented them, along
with some tips on how to avoid them.

https://storm.debian.net/shared/nAY6NRcuFukHvD3fUwtdm3U_2TgJ-8lXuDFch8rhgCF

Please look at this page regularly, as we'll update it throughout the
conference.


Thanks again for helping and enjoy DebConf!

The DebConf Video Team
"""

class Command(BaseCommand):
    help = 'Send email to the video team pointing at the feedback doc'

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),
        parser.add_argument('--username',
                            help='Send mail to a specific user, only'),

    def badger(self, user, dry_run):
        name = user.userprofile.display_name()
        to = user.email

        ctx = {
            'name': name,
        }

        template = engines['django'].from_string(TEMPLATE)
        body = template.render(ctx)

        if dry_run:
            print('Would badger %s <%s>' % (name, to))
            return

        email_message = EmailMultiAlternatives(
            SUBJECT, body, to=["%s <%s>" % (name, to)])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        video_team = Group.objects.get_by_natural_key('Video Team')
        queryset = video_team.user_set.all()
        if options['username']:
            queryset = queryset.filter(username=options['username'])

        for user in queryset:
            self.badger(user, dry_run)
