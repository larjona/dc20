# -*- coding: utf-8 -*-
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.template import engines


SUBJECT = "DebConf20 registration reminder"

TEMPLATE = """\
Dear {{ name }},

You are receiving this because you have created an account on the DebConf 20
website [1], but have either not registered for the conference, or have
registered, but not provided a t-shirt size and shipping address.

If you have not registered, and would like to attend, please do so at
<https://debconf20.debconf.org/register/>.

In order to receive a t-shirt, please provide us with a shipping
address including recipient name, as well as a phone number we can provide to
the shipping company for notifications.  That doesn't need to be your home
address, but it does need to be an address where the parcel will reach you.
You can do so by editing your registration on the DebConf20 website:
<https://debconf20.debconf.org/register/>

Fill in the "Shipping Address" field on the "Personal Information" page.
The deadline to submit your address is 23:59 UTC on Sunday the 26th of July.
We do appreciate that it is very short notice at this point.

We only have a limited budget, and we may not be able to deliver to every
country in time, but we'll do our best.

Extra shirts will be available for purchase; please contact registration if
you'd like to order.

If you no longer plan to attend, please cancel your registration by visiting
<https://debconf20.debconf.org/register/unregister>.

All the best,
The DebConf20 registration team

[1] https://debconf20.debconf.org/
"""

class Command(BaseCommand):
    help = 'Send registration and tshirt reminder email'

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something'),
        parser.add_argument('--username',
                            help='Send mail to a specific user, only'),

    def badger(self, user, dry_run):
        name = user.userprofile.display_name()
        to = user.email

        ctx = {
            'name': name,
        }

        template = engines['django'].from_string(TEMPLATE)
        body = template.render(ctx)

        if dry_run:
            print('Would badger %s <%s>' % (name, to))
            return

        email_message = EmailMultiAlternatives(
            SUBJECT, body, to=["%s <%s>" % (name, to)])
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        User = get_user_model()
        queryset = User.objects.filter(
            Q(attendee__isnull=True)
            | Q(attendee__completed_register_steps__lt=8)
            | Q(attendee__t_shirt_size='')
            | Q(attendee__shipping_address=''))

        if options['username']:
            queryset = queryset.filter(username=options['username'])

        for user in queryset:
            self.badger(user, dry_run)
