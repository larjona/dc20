---
name: Bursaries
---
# Applying for a DebConf20 Online Bursary

With DebConf20 going online, we do not have the traditional bursary categories
for travel, food and accommodation. However, we realize that even entirely
online events are a challenge for some.

This way, DebConf can reimburse up to **50 USD** for costs related to your
DebConf attendance; be it paying for extra internet access, or food you require
during the conference, etc. If you are facing financial challenges due to
COVID-19, consider requesting a DebConf20 Online bursary. We will provide in
the near future detailed instructions on how to request reimbursement after
DebConf, but in the meantime make sure to keep receipts of the expenses you
incur for attending DebConf.

Some attendees might require a higher value. This can be because internet is
specially expensive in their region, or because they need to buy some audio or
video equipment. Such requests should be made directly to the Debian Project
Leader following the usual
[Debian reimbursement process](https://wiki.debian.org/Teams/DPL/Reimbursement).

There are a few different types of bursary available for DebConf, funded from
different budgets:

* Debian funded bursaries are available to active Debian contributors.
* Debian diversity bursaries are available to newcomers to Debian/DebConf,
  especially from under-represented communities. See below.

## Debian funded Bursaries

Any member of the Debian community is welcome to request a bursary
(sponsorship) to attend DebConf. They must inform about their Debian
contributions in the dedicated registration fields.

## Diversity Bursaries

The goal of Diversity Sponsorships is to bring new people to DebConf — people
just getting started in free software or with Debian, contributors who are
making non-coding contributions, and contributors who are trying to make it to
their first DebConf. If you're interested in being considered for a diversity
sponsorship, please tell us a bit about yourself and why you want to come to
DebConf:

* Is this your chance to make your first contribution?
* Do you run Debian and want to learn more about the project and how to get
  involved?
* Are you passionate about free software, and excited to be a part of the Debian
  community?

We would especially like to hear from:

* Women (cis, trans, queer), non-binary and genderqueer individuals, and
  transmen
* Residents from African countries, countries outside of Australia, North
  America, and Western Europe, and people from racial/ethnic minorities in their
  country of residence
* Individuals over the age of 35
