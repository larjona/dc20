---
name: Call for Proposals
---

<!--
From: N. N.
To: debian-devel-announce@lists.debian.org, debconf-announce@lists.debian.org
Subject: Call for Proposals: DebConf20, Online
-->

The DebConf Content team would like to call for proposals for the DebConf20
conference, which will take place online, from August 23rd to 29th, 2020.

<!--
You can find this Call for Proposals, in its latest form, online:
  https://debconf20.debconf.org/cfp/
-->

## Submitting an Event

You can now submit an [event proposal]. Events are not limited to
traditional presentations or informal sessions (BoFs): we welcome
submissions of tutorials, performances, art installations, debates, or any
other format of event that you think would be of interest to the Debian
community.

Regular sessions may either be 20 or 45 minutes long (including time for
questions), other kinds of sessions (workshops, demos, lightning talks,
...) could have different durations. Please choose the most suitable
duration for your event and explain any special requests.

In order to submit a talk, you will need to create an account on the site.
We suggest that Debian Salsa account holders (including DDs and DMs) use
their [Salsa] login when creating an account. However, this isn't required,
as you can sign up with an e-mail address and password.

[event proposal]: https://debconf20.debconf.org/talks/new/
[Salsa]: https://salsa.debian.org/

## Timeline

* Submission deadline: Sunday, July 5th
* Acceptance notification: Monday, July 20th

Late submissions are possible and will be considered on a case by case basis.

## Topics and Tracks

Though we invite proposals on any Debian or FLOSS related subject, we have
some broad topics on which we encourage people to submit proposals,
including but not limited to:

- Introduction to Free Software & Debian
- Packaging, policy, and Debian infrastructure
- Systems administration, automation and orchestration
- Cloud and containers
- Security
- Community, diversity, local outreach and social context
- Internationalization, Localization and Accessibility
- Embedded & Kernel
- Debian in Arts & Science
- Debian Blends and Debian derived distributions

You are welcome to either suggest more tracks, or to become a coordinator
for any of them. For more information, see the [Content team wiki].

[Content team wiki]: https://wiki.debian.org/DebConf/20/Content

## Language-specific miniconfs

This time we will try something different: language-specific miniconfs as
tracks inside DebConf20. If you would like to organize one, get in touch with
the content team by email to <content@debconf.org> or on the #debconf-content
channel on IRC. Note that for that to work, you will have to provide volunteers
who speak the language to the content team (to review and approve talks) and to
the video team (to operate the streaming tools live during the talks or edit
pre-recorded talks in advance), because those jobs will need people with
knowledge of the language spoken in the talks.

## Time zones

As a global online conference, scheduling needs to fit around participants'
time zones.  Please indicate the time zones you'll be available to host your
events from. And if you have expected participants (e.g. for a BoF) please
indicate relevant time zones or a preferred time range (in UTC).

## Online Talks

We are still working on specifics of how exactly the talks will happen, but the
accepted speakers will be notified as soon as we know about that. In principle,
there are 2 possibilities: you can either pre-record your talk and be available
for a live Q&A session, or give it live over videoconferencing.

## Talk proposal help on IRC

This year we will be holding office hours on IRC. Those will be
designated times where the DebConf content team will be available to help
potential speakers prepare their talk proposals for DebConf.

Dates and times for those will be announced later.

## Code of Conduct

Our event is covered by a [Code of Conduct] designed to ensure everyone’s
safety and comfort. The code applies to all attendees, including speakers
and the content of their presentations. Do not hesitate to contact us at
<content@debconf.org> if you have any questions or are unsure about certain
content you’d like to present.

[Code of Conduct]: https://debconf.org/codeofconduct.shtml

## Video Coverage

As an online conference, talks will be streamed live over the Internet. And
unless speakers opt-out, all scheduled talks will be recorded and published
later under the [DebConf license] (MIT/Expat), as well as presentation slides
and papers whenever available.

Please note that, although we will make your wishes known to the participants,
we cannot control whether attendees record and publish your talk from the live
stream.

[conference goals]: https://debconf.org/goals.shtml
[DebConf license]:  https://meetings-archive.debian.net/pub/debian-meetings/LICENSE

## Closing note

DebConf20 is accepting sponsors; if you are interested, or think you know
of others who would be willing to help, please get in touch with
<sponsors@debconf.org>.

In case of any questions, or if you wanted to bounce some ideas off us
first, please do not hesitate to reach out to the content team at
<content@debconf.org>.

We hope to see you online soon!

The DebConf team
